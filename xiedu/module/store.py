import pymysql

class Store:
    "存储相关类"

    conn = None

    # 初始化
    def init():
        Store.conn = pymysql.connect(
            host='127.0.0.1',
            user='root',
            password='yazi123',
            db='xiedu',
            charset='utf8mb4',
            cursorclass=pymysql.cursors.DictCursor
        )

    # 存储数据到数据库
    def save(info):

        try:
            with Store.conn.cursor() as cursor:
                cursor.executemany("insert into content(title, text) values(%s, %s)", info)
            Store.conn.commit()
            return True
        except:
            return False

