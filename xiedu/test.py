
# from multiprocessing import Pool 多进程
from multiprocessing.dummy import Pool as ThreadPool #多线程

urls = [    
    'http://www.python.org',
    'http://www.python.org/about/',
    'http://www.onlamp.com/pub/a/python/2003/04/17/metaclasses.html',
    'http://www.python.org/doc/',
    'http://www.python.org/download/',
    'http://www.python.org/getit/',
    'http://www.python.org/community/',
    'https://wiki.python.org/moin/',
    'http://planet.python.org/',
    'https://wiki.python.org/moin/LocalUserGroups',
    'http://www.python.org/psf/',
    'http://docs.python.org/devguide/',
    'http://www.python.org/community/awards/'
]

def handle(url):
    print("这是一个线程: " + url)

pool = ThreadPool()
pool.map(handle, urls)
pool.close()
pool.join()
