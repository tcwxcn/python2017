import time

# 其他语言的方法
# while True:
#     for x in range(10):
#         print(x)
#         break 2

#     time.sleep(1)

# 用标记变量来判断是否跳出循环
# flag = True
# while flag:
#     for x in range(10):
#         print(x)
#         flag = False
#         break

#     time.sleep(1)

# 使用函数配合return语句实现
# def test():
#     while True:
#         for x in range(10):
#             print(x)
#             return

#         time.sleep(1)

# test()

# 利用else关键字实现跳出多层循环
# i = 0
# while i < 10:
#     i += 1
#     print(i)
#     break
# else:
#     print("hello yazi!")

while True:
    for x in range(10):
        print(x)
        break
    else:
        continue

    break
