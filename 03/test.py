# Tuple（元组）
# List（列表）
# Dictionary（字典）

# Sets（集合）

# t1 = (1, )
# t2 = (1, 2, 3)

# print(type(t1))
# print(type(t2))

# print(    3 * (2 + 5)    )

# 3 * 2 + 5   21
# 3 * 2 + 5   11

# print( (1, ) + (1, 2, 3) )
# print( (1, 2, 3) * 5 )

# t3 = ( 1, 2, 3, 4, 5, 6 )
# print(t3[1:-2])

# print(len(t3))

# l1 = [1, 2, 3, 4]
# l1.append(5)
# print(l1)

# t5 = (1, 2, 3)
# d1 = {"name": "yazi"}
# print(t5[1])
# print(d1["name"])

# 索引数组
# 关联数组

s1 = set( (1, 2, 3, 4) )
s1.add(9)
print(s1)