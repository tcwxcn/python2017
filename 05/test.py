# int("abcd")

# open("aaaaa", "r")

# int
# str
# list
# tuple
# dict
# set

# print(type(str(123)))


# ValueError
# FileNotFoundError

try:
    int("abc")
except ValueError:
    print("Value 异常")
except FileNotFoundError:
    print("File 异常")
else:
    print("没有异常")
finally:
    print("最后执行")

print("程序继续执行")