import time
import random
import threading

# threading.Thread(target=None, args=())

def hello():
    time.sleep(random.random())
    print("hello world %i" % threading.active_count())

def wait():
    for x in arr:
        x.join()
    print("所有线程执行完啦")

arr = []
for x in range(10):
    t = threading.Thread(target = hello)
    t.start()
    arr.append(t)

threading.Thread(target = wait).start()
print("没有阻塞")

# 自定义线程数量 = 所有线程数量 - 1