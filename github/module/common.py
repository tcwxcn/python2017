class Common:

    # 取中间的字符串
    def getMiddle(text, left, right):
        l = text.find(left) + len(left)
        r = text.find(right, l)
        return text[l:r]

