from queue import Queue

class Log:
    "简易日志存储系统"

    q = Queue()

    # 添加日志
    def add(self, content):
        self.q.put(content)

    # 同步
    def sync(self):
        with open("runtime.log", "a", encoding='utf8') as f:
            content = ""
            while(self.q.qsize() > 0):
                content += self.q.get() + "\n"

            f.write(content)
