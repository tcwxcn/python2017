class YStack:
    "亚子使用列表实现的栈"

    arr = []

    # 加一个进去
    def put(self, val):
        self.arr.append(val)

    # 取一个出来
    def get(self):
        return self.arr.pop()

    # 看看栈长度
    def size(self):
        return len(self.arr)

q = YStack()
q.put("亚")
q.put("子")

print(q.size())

print(q.get())
print(q.get())