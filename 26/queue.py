class YQueue:
    "亚子使用列表实现的队列"

    arr = []

    # 加一个进去
    def put(self, val):
        self.arr.append(val)

    # 取一个出来
    def get(self):
        return self.arr.pop(0)

    # 看看队列长度
    def size(self):
        return len(self.arr)

q = YQueue()
q.put("亚")
q.put("子")

print(q.arr)

print(q.size())

print(q.get())
print(q.get())