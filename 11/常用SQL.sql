-- 创建数据库
create database xiedu default character set utf8;

-- 显示当前有哪些数据库
show databases;

-- 我要使用 xiedu 这个数据库
use xiedu

-- 创建数据表
create table content(
    id int unsigned not null auto_increment primary key,
    title varchar(300) not null,
    text text not null
);

-- 查看当前数据库下有哪些表
show tables;

-- 查看某一张表的表结构
desc content;

-- 增删改查
insert into content(title, text) values("测试一个", "测试测试测试测试测试测试测试测试测试");
insert into content values(null, "测试一个", "测试测试测试测试测试测试测试测试测试");

delete from content where id=1;

update content set title="测试二个" where id=1;

select * from content;
select title from content;

-- 重构表
truncate content;

-- 修改 content 表中的 text 列
alter table content modify column text char(15) not null;